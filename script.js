const urlUsers = "https://ajax.test-danit.com/api/json/users";
const urlPosts = "https://ajax.test-danit.com/api/json/posts";

class Card {
	constructor(name, title, body, email, userId, id) {
		this.name = name;
		this.title = title;
		this.body = body;
		this.email = email;
		this.userId = userId;
		this.id = id;
	}
	showPost() {
		const container = document.createElement("div");
		container.classList.add("postCard");
		container.insertAdjacentHTML("afterbegin", [
			`
    <h2>${this.title}</h2>
    <p>${this.body}</p>
    <span>${this.name}</span>
    <span>${this.email}</span>
    <button class="delete">Delete</button>`,
		]);
		container.querySelector(".delete").addEventListener("click", () => {
			fetch(`https://ajax.test-danit.com/api/json/posts/${this.id}`, {
				method: "DELETE",
			}).then(() => {
				container.remove();
				console.log(`Deleted post ${this.id}`);
			});
		});
		document.body.append(container);
	}
}

Promise.all([
	fetch(urlUsers).then((response) => {
		return response.json();
	}),
	fetch(urlPosts).then((response) => {
		return response.json();
	}),
]).then((responses) => {
	const users = responses[0];
	const posts = responses[1];
	createCardPost(posts, createCardUser(users));
});

const createCardUser = function (users) {
	const createPersonalInfo = users.map((element) => {
		const { id, name, email } = element;
		return { id, name, email };
	});
	return createPersonalInfo;
};

const createCardPost = function (posts, createCardUser) {
	const createPersonalPost = posts.map((element) => {
		const personalPost = createCardUser.find(({ id }) => id === element.userId);
		return { ...personalPost, ...element };
	});
	createPersonalPost.forEach((element) => {
		const { name, title, body, email, userId, id } = element;
		const card = new Card(name, title, body, email, userId, id);
		card.showPost();
	});
};
